(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('ngCurio.config', [])
      .value('ngCurio.config', {
          debug: true
      });

  // Modules
  angular.module('ngCurio.services', ['LocalStorageModule']);

  angular.module('ngCurio',
      [
          'ngCurio.config',
          'ngCurio.services',
          'ngSanitize'
      ]);

})(angular);

'use strict';

var mod = angular.module('ngCurio.services');

mod.constant('$curioConstants', {
	LOGOUT: 4,
	LOGIN: 5,
	USER_DATA: 7,
	OPEN_UC: 11
});

mod.factory('$curioAuth',['$curioRequest', '$q', 'localStorageService', '$curioConstants',
	function($curioRequest, $q, localStorageService, $curioConstants) {

		var curioAuth = {};
		var user;

		curioAuth.updateSession = function(context) {
			localStorageService.set('context', context);
		};

		curioAuth.getAuth = function() {
			var context = $curioRequest.getSessionContext();
			if (context === undefined) {
				context = localStorageService.get('context');
				if (context !== undefined || context !== null) {
					$curioRequest.setSessionContext(angular.fromJson(context));
				} else {
					context = undefined;
				}
			}
			return context;
		};

		curioAuth.cleanAuth = function() {
			localStorageService.remove('context');
			localStorageService.remove('user');
			this.user = {
				name: '',
				nature: 'anonymous'
			};
		};

		curioAuth.isAuthenticated = function () {
			return (this.getAuth() !== undefined || this.getAuth() !== null);
		};

		curioAuth.isAuthorized = function (authorizedNatures) {
			if (!angular.isArray(authorizedNatures)) {
				authorizedNatures = [authorizedNatures];
			}
			console.log('user role: ' + this.user.nature + ' authorized: ' + authorizedNatures[0]);
			return authorizedNatures.indexOf(this.user.nature) !== -1;
		};

		curioAuth.login = function(credentials) {
			var self = this;
			var deferred = $q.defer();

			$curioRequest.openSession(credentials).then(
				function (response) {
					console.log(response);
					localStorageService.set('context', response);
					$curioRequest.getUserData(credentials).then(
						function(data) {
							self.user = {};
							self.user.id = data.SYSMSG.ID[0]._;
							self.user.login = data.SYSMSG.Name[0]._;
							if (data.SYSMSG.Email !== undefined && data.SYSMSG.Email[0] !== null) {
								self.user.email = data.SYSMSG.Email[0]._;
							}
							if (data.SYSMSG.Profile !== undefined) {
								self.user.nature = data.SYSMSG.Profile[0]._;
							} else {
								self.user.nature = 'unknown';
							}

							localStorageService.set('user', self.user);
							deferred.resolve(self.user);
						},
						function(err) {
							deferred.reject(err);
						}
					);
				},
				function (err) {
					deferred.reject(err);
				}
			);
			return deferred.promise;
		};

		curioAuth.logout = function() {

			var deferred = $q.defer();

			$curioRequest.closeSession().then(
				function (response) {
					curioAuth.cleanAuth();
					deferred.resolve(true);
				},
				function (err) {
					deferred.reject(err);
				}
			);
			return deferred.promise;
		};

		curioAuth.getUser = function () {
			if (this.user === undefined) {
				this.user = localStorageService.get('user');
				if (this.user === undefined || this.user === null) {
					this.user = {
						name: '',
						nature: 'anonymous'
					};
				} else  {
					this.user = angular.fromJson(this.user);
				}
			}
			return this.user;
		};

		$curioRequest.registerOnContextUpdate(curioAuth.updateSession);
		curioAuth.getAuth();
		curioAuth.getUser();
		return curioAuth;
	}
]);

'use strict';

var mod = angular.module('ngCurio.services');

mod.provider('$curioRequest', [
	function() {

		var url = '';
		var server = 'localhost';
		var syscode = '';
		var port = '';
		var channel = '';
		var timeout = 100000; //default
		var config = {};

		var updateConfig = function() {

			config = {
				headers: {
					'Content-type': 'application/json',
					'cxSystemCode': syscode,
					'cxServer': server,
					'cxPort': port
				}
			};

		};

		this.setUrl = function(value) {
			url = value;
		};

		this.setServer = function(value) {
			server = value;
			updateConfig();
		};

		this.setSysCode = function(value) {
			syscode = value;
			updateConfig();
		};

		this.setPort = function(value) {
			port = value;
			updateConfig();
		};

		this.setChannel = function(value) {
			channel = value;
		};

		this.setTimeout = function(value) {
			timeout = value;
		};

		this.$get = function($http, $q, $rootScope, $curioConstants, $timeout) {

			var context;
			var contextListener;
			var timer;

			var onConnectionError;
			var onTimeoutError;
			var onContextUpdate;

			var curioRequest = {};

			curioRequest.registerOnConnectionError = function (callback) {
				this.onConnectionError = callback;
			};

			curioRequest.registerOnTimeoutError = function (callback) {
				this.onTimeoutError = callback;
			};

			curioRequest.registerOnContextUpdate = function (callback) {
				this.onContextUpdate = callback;
			};

			curioRequest.getSessionContext = function () {
				return this.context;
			};

			curioRequest.setSessionContext = function(ctx) {
				this.context = ctx;
			};

			curioRequest.initSessionContext = function() {
				var ctx = {
					mainUCid: undefined,
					openUC: {}
				};

				return ctx;
			};

			curioRequest.resetSessionContext = function() {
				this.context = undefined;
			};

			curioRequest.updateContext = function() {
				if (this.onContextUpdate !== undefined) {
					this.onContextUpdate(this.getSessionContext());
				}
			};

			curioRequest.getSessionContext = function() {
				return this.context;
			};

			curioRequest.newRequest = function(ucid, signal) {
				var req = {};
				req.ucid = ucid;
				req.signal = signal;

				req.data = {
					SYSMSG: {
						_SignalName: undefined,
						_SerialNumber: undefined,
						_Recipient: undefined,
						_Sender: undefined
					}
				};
				req.send = function() {
					return curioRequest.callUseCase(this.ucid, this.signal, this.data);
				};
				return req;
			};

			curioRequest.execute = function(data) {
				var self = this;
				var deferred = $q.defer();
				console.log('execute:');
				console.log(data);
				$http.post(url, data, config).success(
					function(response) {
						console.log(response);
						if (response.SYSMSG._Id === 1) { //SYSTEM ERROR
							if (response.SYSMSG.MESSAGE[0]._.indexOf('Destinario da requisição não encontrado') > -1) {
								self.resetSessionContext();
								if (self.onTimeoutError) {
									self.onTimeoutError(response.SYSMSG.MESSAGE[0]._);
								}
							}
							deferred.reject(response.SYSMSG.MESSAGE[0]._);
						}
						else if (response.SYSMSG._Id === 2) {
							deferred.reject(response.SYSMSG.MESSAGE[0]._);
						}
						else {

							deferred.resolve(response);
						}
						//timeout
						if (self.timer !== undefined) {
							console.log('canceling timeout');
							$timeout.cancel(self.timer);
						}
						console.log('reset timeout');
						self.timer = $timeout(
							function() {
								console.log('timeout reached');
								self.resetSessionContext();
								self.onTimeoutError('Tempo limite da sessão expirado');
							},
							timeout
						);
					}
				).error(
					function(err) {
						//calback

						//timeout
						if (self.timer !== undefined) {
							console.log('canceling timeout2');
							$timeout.cancel(self.timer);
						}

						if (self.onConnectionError) {
							self.onConnectionError(err);
						}
						deferred.reject(err);
					}
				);
				return deferred.promise;
			};

			curioRequest.getUserData = function(auth) {
				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.USER_DATA,
						_Recipient: 0,
						SYSTEM_CODE: syscode,
						LOGIN: auth.username,
						AUDIT_CONTEXT: '',
						CLIENT_VERSION: '1.0.5.0',
						CHANNEL: channel
					}
				};

				self.execute(data).then(
					function (response) {
						deferred.resolve(response);
					},
					function (err) {
						deferred.reject(err);
					}
				);

				return deferred.promise;
			};

			curioRequest.openSession = function(auth) {

				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.LOGIN,
						_Recipient: 0,
						SYSTEM_CODE: syscode,
						LOGIN: auth.username,
						PASSWORD: auth.password,
						AUDIT_CONTEXT: '',
						CLIENT_VERSION: '1.0.5.0',
						CHANNEL: channel
					}
				};

				console.log(data);

				self.execute(data).then(
					function (response) {
						self.context = self.initSessionContext();
						self.context.mainUCid = response.SYSMSG.UCID[0]._;
						deferred.resolve(self.context);
					},
					function (err) {
						deferred.reject(err);
					}
				);
				return deferred.promise;
			};

			curioRequest.closeSession 	= function() {

				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.LOGOUT,
					}
				};

				self.execute(data).then(
					function (response) {
						self.resetSessionContext();
						deferred.resolve(true);
					},
					function (err) {
						deferred.reject(err);
					}
				);

				return deferred.promise;
			};

			curioRequest.openUseCase = function(ucid) {
				var data = {
					SYSMSG: {
						_Recipient: this.context.mainUCid,
						_SignalName: $curioConstants.OPEN_UC,
						USECASEID: ucid,
						GUIID: ucid
					}
				};
				return this.execute(data);
			};

			curioRequest.callUseCase = function(ucid, signal, msg) {
				var self = this;
				var deferred = $q.defer();

				console.log(self.context);

				if (self.context !== undefined) {
					console.log(self.context.openUC);
					if (self.context.openUC[ucid] === undefined) {
						//must open UC first
						self.openUseCase(ucid).then(
							function(response) {
								console.log('uc opened');
								console.log(response);

								self.context.openUC[ucid] = response.SYSMSG.UCID[0]._; //store Running UC Id
								self.updateContext();

								msg.SYSMSG._SignalName  	= signal;
								msg.SYSMSG._Recipient   	= self.context.openUC[ucid];
								self.execute(msg).then(
									function(resp) {
										deferred.resolve(resp);
									},
									function(err) {
										deferred.reject(err);
									}
								);
							},
							function (err) {
								deferred.reject(err);
							}
						);
					} else {
						msg.SYSMSG._SignalName  = signal;
						msg.SYSMSG._Recipient   = self.context.openUC[ucid];
						self.execute(msg).then(
							function(resp) {
								deferred.resolve(resp);
							},
							function(err) {
								deferred.reject(err);
							}
						);
					}
				} else {
					deferred.reject('not authenticated');
				}
				return deferred.promise;
			};
			return curioRequest;
		};
	}
]);

'use strict';

var mod = angular.module('ngCurio.services');

mod.factory('$curioRequestInterceptor',['$injector',
	function ($injector) {
		var curioInterceptor = {};
		curioInterceptor.request = function (req) {
			var context = $injector.get('$curioAuth').getAuth();
			if (req.method === 'POST' && req.data!==undefined) {
				if (req.data.SYSMSG._SerialNumber === undefined) {
					req.data.SYSMSG._SerialNumber = 0;
				}
				if (req.data.SYSMSG._Sender === undefined) {
					req.data.SYSMSG._Sender = 0;
				}
				if (req.data.SYSMSG._Recipient === undefined) {
					if (context !== undefined) {
						req.data.SYSMSG._Recipient = context.mainUCid;
					} else {
						req.data.SYSMSG._Recipient = 0;
					}
				}
			}
			return req;
		};
		return curioInterceptor;
	}
]);

mod.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('$curioRequestInterceptor');
}]);

mod.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('auth');
  localStorageServiceProvider.setStorageType('localStorage');
  localStorageServiceProvider.setNotify(true, true);
}]);

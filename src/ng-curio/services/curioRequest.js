'use strict';

var mod = angular.module('ngCurio.services');

mod.provider('$curioRequest', [
	function() {

		var url = '';
		var server = 'localhost';
		var syscode = '';
		var port = '';
		var channel = '';
		var timeout = 100000; //default
		var config = {};

		var updateConfig = function() {

			config = {
				headers: {
					'Content-type': 'application/json',
					'cxSystemCode': syscode,
					'cxServer': server,
					'cxPort': port
				}
			};

		};

		this.setUrl = function(value) {
			url = value;
		};

		this.setServer = function(value) {
			server = value;
			updateConfig();
		};

		this.setSysCode = function(value) {
			syscode = value;
			updateConfig();
		};

		this.setPort = function(value) {
			port = value;
			updateConfig();
		};

		this.setChannel = function(value) {
			channel = value;
		};

		this.setTimeout = function(value) {
			timeout = value;
		};

		this.$get = function($http, $q, $rootScope, $curioConstants, $timeout) {

			var context;
			var contextListener;
			var timer;

			var onConnectionError;
			var onTimeoutError;
			var onContextUpdate;

			var curioRequest = {};

			curioRequest.registerOnConnectionError = function (callback) {
				this.onConnectionError = callback;
			};

			curioRequest.registerOnTimeoutError = function (callback) {
				this.onTimeoutError = callback;
			};

			curioRequest.registerOnContextUpdate = function (callback) {
				this.onContextUpdate = callback;
			};

			curioRequest.getSessionContext = function () {
				return this.context;
			};

			curioRequest.setSessionContext = function(ctx) {
				this.context = ctx;
			};

			curioRequest.initSessionContext = function() {
				var ctx = {
					mainUCid: undefined,
					openUC: {}
				};

				return ctx;
			};

			curioRequest.resetSessionContext = function() {
				this.context = undefined;
			};

			curioRequest.updateContext = function() {
				if (this.onContextUpdate !== undefined) {
					this.onContextUpdate(this.getSessionContext());
				}
			};

			curioRequest.getSessionContext = function() {
				return this.context;
			};

			curioRequest.newRequest = function(ucid, signal) {
				var req = {};
				req.ucid = ucid;
				req.signal = signal;

				req.data = {
					SYSMSG: {
						_SignalName: undefined,
						_SerialNumber: undefined,
						_Recipient: undefined,
						_Sender: undefined
					}
				};
				req.send = function() {
					return curioRequest.callUseCase(this.ucid, this.signal, this.data);
				};
				return req;
			};

			curioRequest.execute = function(data) {
				var self = this;
				var deferred = $q.defer();
				console.log('execute:');
				console.log(data);
				$http.post(url, data, config).success(
					function(response) {
						console.log(response);
						if (response.SYSMSG._Id === 1) { //SYSTEM ERROR
							if (response.SYSMSG.MESSAGE[0]._.indexOf('Destinario da requisição não encontrado') > -1) {
								self.resetSessionContext();
								if (self.onTimeoutError) {
									self.onTimeoutError(response.SYSMSG.MESSAGE[0]._);
								}
							}
							deferred.reject(response.SYSMSG.MESSAGE[0]._);
						}
						else if (response.SYSMSG._Id === 2) {
							deferred.reject(response.SYSMSG.MESSAGE[0]._);
						}
						else {

							deferred.resolve(response);
						}
						//timeout
						if (self.timer !== undefined) {
							console.log('canceling timeout');
							$timeout.cancel(self.timer);
						}
						console.log('reset timeout');
						self.timer = $timeout(
							function() {
								console.log('timeout reached');
								self.resetSessionContext();
								self.onTimeoutError('Tempo limite da sessão expirado');
							},
							timeout
						);
					}
				).error(
					function(err) {
						//calback

						//timeout
						if (self.timer !== undefined) {
							console.log('canceling timeout2');
							$timeout.cancel(self.timer);
						}

						if (self.onConnectionError) {
							self.onConnectionError(err);
						}
						deferred.reject(err);
					}
				);
				return deferred.promise;
			};

			curioRequest.getUserData = function(auth) {
				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.USER_DATA,
						_Recipient: 0,
						SYSTEM_CODE: syscode,
						LOGIN: auth.username,
						AUDIT_CONTEXT: '',
						CLIENT_VERSION: '1.0.5.0',
						CHANNEL: channel
					}
				};

				self.execute(data).then(
					function (response) {
						deferred.resolve(response);
					},
					function (err) {
						deferred.reject(err);
					}
				);

				return deferred.promise;
			};

			curioRequest.openSession = function(auth) {

				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.LOGIN,
						_Recipient: 0,
						SYSTEM_CODE: syscode,
						LOGIN: auth.username,
						PASSWORD: auth.password,
						AUDIT_CONTEXT: '',
						CLIENT_VERSION: '1.0.5.0',
						CHANNEL: channel
					}
				};

				console.log(data);

				self.execute(data).then(
					function (response) {
						self.context = self.initSessionContext();
						self.context.mainUCid = response.SYSMSG.UCID[0]._;
						deferred.resolve(self.context);
					},
					function (err) {
						deferred.reject(err);
					}
				);
				return deferred.promise;
			};

			curioRequest.closeSession 	= function() {

				var self = this;

				var deferred = $q.defer();

				var data = {
					SYSMSG: {
						_SignalName: $curioConstants.LOGOUT,
					}
				};

				self.execute(data).then(
					function (response) {
						self.resetSessionContext();
						deferred.resolve(true);
					},
					function (err) {
						deferred.reject(err);
					}
				);

				return deferred.promise;
			};

			curioRequest.openUseCase = function(ucid) {
				var data = {
					SYSMSG: {
						_Recipient: this.context.mainUCid,
						_SignalName: $curioConstants.OPEN_UC,
						USECASEID: ucid,
						GUIID: ucid
					}
				};
				return this.execute(data);
			};

			curioRequest.callUseCase = function(ucid, signal, msg) {
				var self = this;
				var deferred = $q.defer();

				console.log(self.context);

				if (self.context !== undefined) {
					console.log(self.context.openUC);
					if (self.context.openUC[ucid] === undefined) {
						//must open UC first
						self.openUseCase(ucid).then(
							function(response) {
								console.log('uc opened');
								console.log(response);

								self.context.openUC[ucid] = response.SYSMSG.UCID[0]._; //store Running UC Id
								self.updateContext();

								msg.SYSMSG._SignalName  	= signal;
								msg.SYSMSG._Recipient   	= self.context.openUC[ucid];
								self.execute(msg).then(
									function(resp) {
										deferred.resolve(resp);
									},
									function(err) {
										deferred.reject(err);
									}
								);
							},
							function (err) {
								deferred.reject(err);
							}
						);
					} else {
						msg.SYSMSG._SignalName  = signal;
						msg.SYSMSG._Recipient   = self.context.openUC[ucid];
						self.execute(msg).then(
							function(resp) {
								deferred.resolve(resp);
							},
							function(err) {
								deferred.reject(err);
							}
						);
					}
				} else {
					deferred.reject('not authenticated');
				}
				return deferred.promise;
			};
			return curioRequest;
		};
	}
]);

'use strict';

var mod = angular.module('ngCurio.services');

mod.factory('$curioRequestInterceptor',['$injector',
	function ($injector) {
		var curioInterceptor = {};
		curioInterceptor.request = function (req) {
			var context = $injector.get('$curioAuth').getAuth();
			if (req.method === 'POST' && req.data!==undefined) {
				if (req.data.SYSMSG._SerialNumber === undefined) {
					req.data.SYSMSG._SerialNumber = 0;
				}
				if (req.data.SYSMSG._Sender === undefined) {
					req.data.SYSMSG._Sender = 0;
				}
				if (req.data.SYSMSG._Recipient === undefined) {
					if (context !== undefined) {
						req.data.SYSMSG._Recipient = context.mainUCid;
					} else {
						req.data.SYSMSG._Recipient = 0;
					}
				}
			}
			return req;
		};
		return curioInterceptor;
	}
]);

mod.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('$curioRequestInterceptor');
}]);

mod.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('auth');
  localStorageServiceProvider.setStorageType('localStorage');
  localStorageServiceProvider.setNotify(true, true);
}]);

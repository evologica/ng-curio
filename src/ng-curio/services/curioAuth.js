'use strict';

var mod = angular.module('ngCurio.services');

mod.constant('$curioConstants', {
	LOGOUT: 4,
	LOGIN: 5,
	USER_DATA: 7,
	OPEN_UC: 11
});

mod.factory('$curioAuth',['$curioRequest', '$q', 'localStorageService', '$curioConstants',
	function($curioRequest, $q, localStorageService, $curioConstants) {

		var curioAuth = {};
		var user;

		curioAuth.updateSession = function(context) {
			localStorageService.set('context', context);
		};

		curioAuth.getAuth = function() {
			var context = $curioRequest.getSessionContext();
			if (context === undefined) {
				context = localStorageService.get('context');
				if (context !== undefined || context !== null) {
					$curioRequest.setSessionContext(angular.fromJson(context));
				} else {
					context = undefined;
				}
			}
			return context;
		};

		curioAuth.cleanAuth = function() {
			localStorageService.remove('context');
			localStorageService.remove('user');
			this.user = {
				name: '',
				nature: 'anonymous'
			};
		};

		curioAuth.isAuthenticated = function () {
			return (this.getAuth() !== undefined || this.getAuth() !== null);
		};

		curioAuth.isAuthorized = function (authorizedNatures) {
			if (!angular.isArray(authorizedNatures)) {
				authorizedNatures = [authorizedNatures];
			}
			console.log('user role: ' + this.user.nature + ' authorized: ' + authorizedNatures[0]);
			return authorizedNatures.indexOf(this.user.nature) !== -1;
		};

		curioAuth.login = function(credentials) {
			var self = this;
			var deferred = $q.defer();

			$curioRequest.openSession(credentials).then(
				function (response) {
					console.log(response);
					localStorageService.set('context', response);
					$curioRequest.getUserData(credentials).then(
						function(data) {
							self.user = {};
							self.user.id = data.SYSMSG.ID[0]._;
							self.user.login = data.SYSMSG.Name[0]._;
							if (data.SYSMSG.Email !== undefined && data.SYSMSG.Email[0] !== null) {
								self.user.email = data.SYSMSG.Email[0]._;
							}
							if (data.SYSMSG.Profile !== undefined) {
								self.user.nature = data.SYSMSG.Profile[0]._;
							} else {
								self.user.nature = 'unknown';
							}

							localStorageService.set('user', self.user);
							deferred.resolve(self.user);
						},
						function(err) {
							deferred.reject(err);
						}
					);
				},
				function (err) {
					deferred.reject(err);
				}
			);
			return deferred.promise;
		};

		curioAuth.logout = function() {

			var deferred = $q.defer();

			$curioRequest.closeSession().then(
				function (response) {
					curioAuth.cleanAuth();
					deferred.resolve(true);
				},
				function (err) {
					deferred.reject(err);
				}
			);
			return deferred.promise;
		};

		curioAuth.getUser = function () {
			if (this.user === undefined) {
				this.user = localStorageService.get('user');
				if (this.user === undefined || this.user === null) {
					this.user = {
						name: '',
						nature: 'anonymous'
					};
				} else  {
					this.user = angular.fromJson(this.user);
				}
			}
			return this.user;
		};

		$curioRequest.registerOnContextUpdate(curioAuth.updateSession);
		curioAuth.getAuth();
		curioAuth.getUser();
		return curioAuth;
	}
]);

(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('ngCurio.config', [])
      .value('ngCurio.config', {
          debug: true
      });

  // Modules
  angular.module('ngCurio.services', ['LocalStorageModule']);

  angular.module('ngCurio',
      [
          'ngCurio.config',
          'ngCurio.services',
          'ngSanitize'
      ]);

})(angular);

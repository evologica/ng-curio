'use strict'

var app = angular.module('demo', ['ngCurio']);

app.constant('USER_NATURE', {
  anonymous: 'anonymous',
  primeiroAcesso: 4,
  segurado: 5
});

app.controller('DemoCtrl', ['$curioAuth', '$curioRequest', '$scope', 
	function ($curioAuth, $curioRequest, $scope) {

		$scope.message = $curioAuth.getUser();

		$scope.login = function() {

			$scope.message = '';

			var credentials = {
				username: '07031922720',
				password: '0'
			};

			$curioAuth.login(credentials).then(
				function(response) {
					$scope.message = response
				},
				function(err) {
					$scope.message = err;
				}
			);
		};

		$scope.logout = function() {

			$scope.message = '';

			$curioAuth.logout().then(
				function(response) {
					$scope.message = 'logout successful'
				},
				function(err) {
					$scope.message = 'logout unsuccesful';
				}
			);
		};

		$scope.listPolicies = function() {
			$scope.message = '';
			var req = $curioRequest.newRequest(2655, 'RM_CONSULTA_CONTRATOS');
			//req.data.SYSMSG.CPF_CNPJ = '07031922720';
			req.send().then(
				function (response) {
					console.log(response);
					$scope.message = response;
				},
				function (err) {
					$scope.message = err;
				}
			);
		};
	}
]);

app.config(function($curioRequestProvider) {
	$curioRequestProvider.setUrl('http://192.168.0.32/cxIsapi/cxIsapiClient.dll/gatewayJSON');
	$curioRequestProvider.setServer('localhost');
	$curioRequestProvider.setSysCode(63);
	$curioRequestProvider.setPort(5370);
	$curioRequestProvider.setChannel('BANSEG');
	$curioRequestProvider.setTimeout(1500000);
});

app.run(function($curioRequest, $curioAuth) {

		$curioRequest.registerOnTimeoutError(
			function () {
				$curioAuth.cleanAuth();
				console.log('teste');
			}

		);

		$curioRequest.registerOnConnectionError(
			function () {
				$curioAuth.cleanAuth();
				console.log('teste');
			}
		);

	}
);